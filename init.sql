CREATE TABLE bank4all_users (
	id serial not null primary key,
	name text not null,
	email text not null,
	phone_number text not null,
	address text not null,
	national_id text not null,
	birth_date date not null,
	password text not null
);


create table BANK4ALL_BANKS (
	id serial not null primary key,
	name text not null
);


CREATE TABLE bank4all_accounts (
	id serial not null primary key,
	account_number integer not null,
	agency_number integer not null,
        access_token varchar,
	user_id integer references bank4all_users(id) not null,
	bank_id integer references bank4all_banks(id) not null,
	integration_confirmed boolean default false not null,
  	CONSTRAINT account_bank_unique UNIQUE (account_number, bank_id)
);


insert into public.BANK4ALL_BANKS (name) values ('Bradesco');
insert into public.BANK4ALL_BANKS (name) values ('Banco do Brasil');
insert into public.BANK4ALL_BANKS (name) values ('NuBank');
insert into public.BANK4ALL_BANKS (name) values ('Caixa');


