**4CADIA Challenge - Bank4All**

**Description**

This application was designed to help users have a better understanding of their overall financial status and expenses x income, benefiting from Open Banking features to connect multiple bank accounts' data.

**Technologies Used**

Client-side

- Angular 11 / Typescript
- Bootstrap
- Karma / Jasmine
- PrimeNG (Ui)

Backend
- NodeJS
- Express
- NestJS
- Swagger

Other
- Docker

**App Structure**

An angular application consumes a NestJS API, which has endpoints for registering users and their bank accounts, as well as requesting data for those accounts, and listing available banks for linking. A PostgreSQL database stores relevant user data, while a mock API represents what would be each Bank dedicate API, retrieving data generated on-the-fly for debugging purposes.

**Usage Flow**

- A user registers a new account on the app. 
- After logging in with the registered credentials, the user links existing Bank Accounts into the app, using the 'My Accounts' section of the app. 
- After clicking 'New Account', the user fills the data for the account. This creates a link request, which then awaits for the user to accept it on the native app. 
- Currently, a Mock Api receives link requests for all banks, and we can simulate accepting or revoking all tokens using the API interface. When a request for linking is made, the mock api generates a token.
- When a token is accepted using the Mock Api interface, the User Dashboard and Account Details screens will show mocked data for the selected accounts.

Obs.: The Mock Api doesn't persist any data, and all data generated while using the application will be deleted once it is shut down. The data generated is also random, so each request will produce a different result.


**Documentation**

**API**


