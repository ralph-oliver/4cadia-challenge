export class BankAccount {
    id?:string;
    userId?:string;
    accountNumber:string;
    agencyNumber:string;
    bankId:string;
    bankName:string;
    bankData?:string;
}
