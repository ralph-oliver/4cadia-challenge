import { Component, Input, OnInit } from '@angular/core';
import { BankAccountService } from 'src/app/services/bank-account.service';

const successModal = {
  title:'Success',
  text:'A new authorization request was sent.'
};

const errorModal = {
  title:'Error',
  text:'An error ocurred. Please try again later.'
};


@Component({
  selector: 'app-authorization-notice',
  templateUrl: './authorization-notice.component.html',
  styleUrls: ['./authorization-notice.component.scss']
})
export class AuthorizationNoticeComponent implements OnInit {

  @Input()
  accountId:string;

  modalOpen:boolean = false;
  modalModel:any;


  constructor(private accountService:BankAccountService) { }

  ngOnInit(): void {
  }

  makeLinkRequest() {
    this.accountService.makeNewLinkRequest(this.accountId).subscribe(result=>{
      this.showModal(true);
    }, err => {
      this.showModal(false)
    })
  }

  showModal(isSuccess:boolean) {
    this.modalOpen = true;
    this.modalModel = isSuccess? successModal : errorModal;
  }

}
