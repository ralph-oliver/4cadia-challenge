import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-side-area',
  templateUrl: './side-area.component.html',
  styleUrls: ['./side-area.component.scss']
})
export class SideAreaComponent implements OnInit {

  username:string;

  constructor(
    private authService:AuthService,
    private router:Router
    ) {

    this.authService.loginChange.subscribe(result => {
      this.username = this.authService.getLoggedUser();
    })
    
  }

  ngOnInit(): void {
    this.username = this.authService.getLoggedUser();
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('login');
  }

  navigateToHome() {
    this.router.navigateByUrl('home');
  }

  navigateToAccounts() {
    this.router.navigateByUrl('my-accounts');
  }

}
