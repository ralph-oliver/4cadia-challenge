import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-mobile-navbar',
  templateUrl: './mobile-navbar.component.html',
  styleUrls: ['./mobile-navbar.component.scss']
})
export class MobileNavbarComponent implements OnInit {

  items: MenuItem[];
  
  constructor(private router:Router, private authService:AuthService) { }

  ngOnInit(): void {
    this.initializeMenu();
  }

  initializeMenu() {

    this.items = [
      {
        label:'Home',
        command:() => {
          this.router.navigateByUrl('')
        }
      },
      {
        label:'My accounts',
        command:() => {
          this.router.navigateByUrl('my-accounts')
        }
      },
      {
        label:'Logout',
        command:() => {
            this.authService.logout();
            this.router.navigateByUrl('login');
        }
      }
    ];
  }

  openHome() {
    this.router.navigateByUrl('/');
  }

}
