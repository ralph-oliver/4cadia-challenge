import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path:'login',
    loadChildren: ()=> import('./modules/login/login.module').then(m=>m.LoginModule),
  },
  {
    path:'register',
    loadChildren: ()=> import ('./modules/registration/registration.module').then (m=>m.RegistrationModule)
  },
  {
    path:'home',
    loadChildren: ()=> import ('./modules/home/home.module').then (m=>m.HomeModule),
    canLoad:[AuthGuard]
  },
  {
    path:'my-accounts',
    loadChildren: ()=> import ('./modules/accounts/accounts.module').then(m=> m.AccountsModule),
    canLoad:[AuthGuard]
  },
  {
    path:'**',
    redirectTo:'/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
