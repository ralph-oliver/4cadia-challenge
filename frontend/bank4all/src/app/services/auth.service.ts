import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const baseUrl = 'auth/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  user:any;
  loginChange:EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private httpClient:HttpClient) { }

  getLoggedUser() {
    return localStorage.getItem('currentUser');
  }

  logout() {
    this.user = null;
    localStorage.removeItem("token");
    localStorage.removeItem("currentUser");
    this.loginChange.next(false);
  }

  login(userData:any) {
    return this.httpClient.post(environment.apiUrl + baseUrl + 'login', userData);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  setSession(authResult:any) {
    localStorage.setItem('token', authResult.access_token);
    localStorage.setItem('currentUser', authResult.email);
  }
}
