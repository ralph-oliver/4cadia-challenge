import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const baseUrl = 'user/';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient) { }

  registerNewUser(user:any) {
    return this.httpClient.post(environment.apiUrl + baseUrl, user);
  }
}
