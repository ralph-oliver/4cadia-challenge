import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const baseUrl ='bank-account/';

@Injectable({
  providedIn: 'root'
})
export class BankAccountService {

  constructor(private httpClient:HttpClient) { }

  linkNewAccount(linkAccountObj:any) {
    return this.httpClient.post(environment.apiUrl + baseUrl, linkAccountObj);
  }

  getUserAccounts() {
    return this.httpClient.get(environment.apiUrl + baseUrl);
  }

  getAccountData(accountId:string) {
    return this.httpClient.get(environment.apiUrl + baseUrl + 'transactions/' + accountId);
  }

  getAccountBalance(accountId:string) {
    return this.httpClient.get(environment.apiUrl + baseUrl + 'balance/' + accountId);
  }

  makeNewLinkRequest(accountId:string) {
    return this.httpClient.get(environment.apiUrl + baseUrl + 'new-link-request/' + accountId);
  }
}
