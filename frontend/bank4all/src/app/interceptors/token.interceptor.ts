import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, map, Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth:AuthService, private router:Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      }
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => { return event}),
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401 && err.error.message != "INVALID_TOKEN") {
                        this.auth.logout();
                        setTimeout(()=>this.router.navigateByUrl('/login'), 100);
                    }
                }
                return throwError(err);
            })
    );
  }
}
