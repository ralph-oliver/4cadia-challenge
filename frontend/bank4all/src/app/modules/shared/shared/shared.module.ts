import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationNoticeComponent } from 'src/app/shared/components/authorization-notice/authorization-notice.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [AuthorizationNoticeComponent],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule
  ],
  exports:[AuthorizationNoticeComponent]
})
export class SharedModule { }
