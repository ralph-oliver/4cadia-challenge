import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {

  constructor(private userService:UserService, private router:Router) { }

  isActive:boolean = false;
  user = new User();
  password:string;
  repeatedPassword:string;
  passwordMismatch:boolean = false;
  hasErrors:boolean = false;
  errorMessage:string = "Fill all fields correctly."
  showErrors:boolean = false;
  showSuccessModal:boolean = false;

  ngOnInit() {
    this.isActive = true;
  }

  validatePasswords() {
      if (!this.repeatedPassword || !this.password) {
        this.passwordMismatch = false;
        return;
      }

      this.passwordMismatch = this.password != this.repeatedPassword;
  }

  validateForm() {
    this.errorMessage = "";

    this.validatePasswords();

    if (this.passwordMismatch) {
      this.errorMessage = "Passwords do not match."
      return false;
    }

    let valid = 
    this.user.name &&
    this.user.email &&
    this.user.phone_number &&
    this.user.address &&
    this.user.national_id &&
    this.user.birth_date &&
    this.password &&
    this.repeatedPassword;

    if (!valid) {
      this.errorMessage = "Please fill all fields"
      return false;
    }

    return true;  
  }

  register() {
    if (!this.validateForm()) {
      this.showErrors = true;
      return;
    }

    let request = Object.assign({},this.user as any);
    request.password = this.password;

    this.userService.registerNewUser(request).subscribe(result=> {
      this.showSuccessModal = true;
    });
  }

  redirectToHome() {
    this.router.navigateByUrl('/');
  }


}
