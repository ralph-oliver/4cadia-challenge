import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';

import { DropdownModule } from 'primeng/dropdown'
import { ChartModule } from 'primeng/chart';
import { SharedModule } from '../shared/shared/shared.module'

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    DropdownModule,
    ChartModule,
    SharedModule
  ]
})
export class HomeModule { }
