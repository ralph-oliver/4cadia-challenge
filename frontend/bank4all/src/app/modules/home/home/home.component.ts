import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BankAccountService } from 'src/app/services/bank-account.service';
import { BankService } from 'src/app/services/bank.service';
import { BankAccount } from 'src/app/shared/models/bank-account';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  availableBalance:number = 0;
  bankList:any[] = [];
  lineChartData:any;
  unauthorized:boolean = false;
  accountId:string;
  donutChartdata:any;
  noContent:boolean = true;
  showBalance:boolean = false;
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October','November','December'];
  
  constructor(private accountService:BankAccountService, private router:Router) { }

  ngOnInit() {
    this.accountService.getUserAccounts().subscribe((result:BankAccount[])=>{
      if (result.length > 0) {
        this.noContent = false;
        this.bankList = result;
        this.bankList.forEach(x=> x.label = x.bankName + ' - ' + x.accountNumber + ' (Ag. ' + x.agencyNumber + ')');
        this.loadBankData(result[0].id);
      }
    });
  
  }

  loadBankData(accountId:any) {
    this.accountId = accountId;
    this.accountService.getAccountBalance(accountId).subscribe(result => {
      this.availableBalance = result as number;
      this.unauthorized = false;

    }, err => {
      if (err.status == 401) {
        this.unauthorized = true;
      } 
    });

    this.accountService.getAccountData(accountId).subscribe((result:any) => {
        this.createCharts(result.bankData);
    });
  }

  createCharts(bankData:any) {
    let positives = []
    let negatives = []


    this.lineChartData = {
      labels :[],
      datasets: []
    }

    this.donutChartdata = {
      labels:["Income","Expense"],
      datasets: []
    }

    let limitDate = new Date();
    limitDate.setFullYear(limitDate.getFullYear() -1);
    
    bankData = bankData.filter(x=> new Date(x.date) > limitDate);

    for (let i=0;i<4;i++) {

      let currentMonthIndex = new Date().getMonth() -i;
      currentMonthIndex = currentMonthIndex > -1? currentMonthIndex : 12 + currentMonthIndex; 

      let y = bankData.filter(x=>new Date(x.date).getMonth() == currentMonthIndex).map(x=>x.amount).filter(x=>x >= 0);
      let result = y.length? y.reduceRight((a,b)=> a+b) : 0;
      positives.push(result);

      y = bankData.filter(x=>new Date(x.date).getMonth() == currentMonthIndex).map(x=>x.amount).filter(x=>x < 0);
      result = y.length? y.reduceRight((a,b)=> a+b) : 0;
      negatives.push(result);

      this.lineChartData.labels.push(this.monthNames[currentMonthIndex]);
    }

    this.lineChartData.labels.reverse();
    this.lineChartData.datasets.forEach(x=> x.data.reverse());

    this.lineChartData.datasets = 
      [
        {
          label:"Income",
          data:positives.map(x=>x.toFixed(2)),
          backgroundColor: "#0de5b2"
        },
        {
          label:"Expense",
          data:negatives.map(x=>Math.abs(x).toFixed(2)),
          backgroundColor: "#ea3d3d"
        }
      ];

     
    this.donutChartdata.datasets = [
        {
          data:[
            positives.length > 0? [positives.reduceRight((a,b)=>(a+b)).toFixed(2)] : [0],
            negatives.length > 0? [negatives.reduceRight((a,b)=>(a+b)).toFixed(2)] : [0],
          ],
          backgroundColor: [
            "#0de5b2",
            "#ea3d3d"
          ]
        }
      ];
  }

  openAccounts() {
    this.router.navigateByUrl('/my-accounts');
  }



}
