import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { LinkNewAccountComponent } from './link-new-account/link-new-account.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared/shared.module'
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [
    LinkNewAccountComponent,
    AccountDetailsComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule,
    DropdownModule,
    CalendarModule,
    FormsModule,
    SharedModule,
    DialogModule
  ]
})
export class AccountsModule { }
