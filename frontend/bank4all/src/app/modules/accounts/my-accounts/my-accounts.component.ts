import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BankAccountService } from 'src/app/services/bank-account.service';
import { BankAccount } from 'src/app/shared/models/bank-account';

@Component({
  selector: 'app-my-accounts',
  templateUrl: './my-accounts.component.html',
  styleUrls: ['./my-accounts.component.scss']
})
export class MyAccountsComponent implements OnInit {

  accounts:BankAccount[] = [];
  noContent:boolean = false;

  constructor(
    private router:Router,
    private bankAccountService:BankAccountService
    ) { }

  ngOnInit(): void {
    this.bankAccountService.getUserAccounts().subscribe(result => {
      this.accounts = result as BankAccount[];
      this.noContent = this.accounts.length == 0;
    })
  }

  openLinkAccount() {
    this.router.navigateByUrl('/my-accounts/link-account');
  }

  openAccount(id: any) {
    this.router.navigateByUrl('/my-accounts/account-details/'+ id);
  }

}
