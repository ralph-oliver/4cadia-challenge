import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BankAccountService } from 'src/app/services/bank-account.service';
import { BankService } from 'src/app/services/bank.service';
import { BankAccount } from 'src/app/shared/models/bank-account';

@Component({
  selector: 'app-link-new-account',
  templateUrl: './link-new-account.component.html',
  styleUrls: ['./link-new-account.component.scss']
})
export class LinkNewAccountComponent implements OnInit {

  account:BankAccount = new BankAccount();
  banks:any = [];
  currentBank:any;
  modalMessage:string = "";
  isModalSuccess:boolean = false;
  isModalOpen:boolean = false;

  constructor(
    private bankAccountService:BankAccountService,
    private bankService:BankService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.loadBanks();
  }

  loadBanks() {
    this.bankService.getAllBanks().subscribe(result => {
      this.banks = result;
      this.currentBank = result[0];
    })
  }

  createLinkRequest() {
    this.account.bankId = this.currentBank.id;
    
    this.bankAccountService.linkNewAccount(this.account).subscribe((result:any) => {
      this.isModalSuccess = true;
      this.isModalOpen = true;
      this.modalMessage = "Account link request sent successful. You must authorize Banklink in your bank's app or website."
    }, (error:any) => {
      this.modalMessage = "Error linking account. Make sure the account is valid and that it has not been linked yet."
      this.isModalSuccess = false;
      this.isModalOpen = true;
    })
  }

  hideCallback() {
    if (this.isModalSuccess) {
      this.router.navigateByUrl('/my-accounts');
    }
  }

}
