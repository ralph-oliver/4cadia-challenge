import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BankAccountService } from 'src/app/services/bank-account.service';
import { BankService } from 'src/app/services/bank.service';
import { BankAccount } from 'src/app/shared/models/bank-account';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit {

  bankList:any[] = [];
  currentAccount:BankAccount;
  unauthorizedAccount:boolean = false;
  dateFrom:Date;
  accountId:string;
  dateTo:Date;
  
  constructor(private accountService:BankAccountService, private route:ActivatedRoute) {
    
    this.route.params.subscribe(params => {

        this.accountService.getUserAccounts().subscribe((result:BankAccount[])=>{
          this.bankList = result;
          this.bankList.forEach(x=> x.label = x.bankName + ' - Ag:' + x.agencyNumber + ' - ' + x.accountNumber);

          if (params.accountId) {
            this.accountId = params.accountId;
            this.accountService.getAccountData(params.accountId).subscribe(result => {
              this.currentAccount = result as BankAccount;
            }, err => {
              if (err.status == 401) {
                this.unauthorizedAccount = true;
              }
            });
           }

       });
    });
  }

  ngOnInit(): void {
  }

}
