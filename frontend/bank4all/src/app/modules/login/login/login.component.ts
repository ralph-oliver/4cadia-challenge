import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  showErrors = false;
  errorMessage = "";

  loginData = {
    email:'',
    password:''
  };
  
  constructor(
    private authService:AuthService, 
    private router:Router
    ) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.loginData).subscribe((result:any)=> {
      if (result.access_token) {
        this.authService.setSession(result);
        this.router.navigateByUrl('home');
        this.authService.loginChange.next(true);
      }
      else {
        this.showErrors = true;
        this.errorMessage = "Unknown error. Contact the system's administrators."
      }
      
    }, error => {
      this.showErrors = true;
      this.errorMessage = "Incorrect username or password."
    });

  }

}
