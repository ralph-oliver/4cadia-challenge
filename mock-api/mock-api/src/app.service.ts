import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Transaction } from './transaction';

@Injectable()
export class AppService {

  validTokens:string[] = [];
  pendingTokens:string[] = [];
  
  grantToken(): string {
    const token = this.generateToken(100);
    this.pendingTokens.push(token);
    return token;
    
  }

  generateToken(n) {
    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var token = '';
    for(var i = 0; i < n; i++) {
        token += chars[Math.floor(Math.random() * chars.length)];
    }
    return token;
  }

  acceptAllTokens() {
    this.validTokens = [...this.validTokens,...this.pendingTokens];
  }

  revokeAllTokens() {
    this.validTokens = [];
    this.pendingTokens = [];
  }

  getRandomData(token: string) {
    if (!this.validTokens.includes(token)) {
      throw new HttpException("Invalid or expired token.", HttpStatus.UNAUTHORIZED);
    }

    let data:Transaction[] = [];

    for (let i=0;i<20;i++) {
      data[i] = {
        amount:(Math.random()*1000) * (Math.random() < 0.5? 1: -1),
        description:"Transaction text " + i,
        date: new Date(new Date().getTime() - i * (2592000000/2))
      }
    }

    return data;
  }

  
}
