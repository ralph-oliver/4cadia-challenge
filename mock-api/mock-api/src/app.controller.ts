import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('requestAccess')
  requestAccess() {
    return this.appService.grantToken();
  }

  @Get('approveAllTokens')
  approveAllTokens() {
    return this.appService.acceptAllTokens();
  }

  @Get('revokeAllTokens')
  revokeAllTokens() {
    return this.appService.revokeAllTokens();
  }

  @Get('getBankData/:token')
  getBankData(@Param() params) {
  return this.appService.getRandomData(params.token);
  }

  @Get('/')
  renderPage() {
    return `<!DOCTYPE html>
              <head>
                  <title>Mock Api Controller</title>
                  <script
                  src="https://code.jquery.com/jquery-3.6.0.min.js"
                  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                  crossorigin="anonymous"></script>
              </head>
              <body>
                  <button onClick='acceptAllTokens()'>Accept all tokens </a>
                  <button onClick='revokeAllTokens()'>Revoke all tokens </a>
              </body>
              <script>
              document.acceptAllTokens = function () {
                $.get('/approveAllTokens', function(result) {
                  alert('All tokens approved successfully');
                });
              };
              document.revokeAllTokens = function ()  {
                $.get('/revokeAllTokens', function(result) {
                  alert('All tokens revoked successfully');
                });
              }
              </script>
          </html>`;
  }


}
