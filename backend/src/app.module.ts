import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BankAccountModule } from './modules/bank-account/bank-account/bank-account.module';
import { BankModule } from './modules/bank/bank.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    BankModule,
    UserModule,
    BankAccountModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
