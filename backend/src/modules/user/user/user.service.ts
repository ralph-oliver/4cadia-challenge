import { Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION } from 'src/constants';
import { UserDTO } from './dto/user-dto';
import { hash, genSalt } from 'bcrypt';

const saltRounds = 10;
@Injectable()
export class UserService {

    constructor(@Inject(PG_CONNECTION) private conn: any) {
    }

    async createUser(user:UserDTO) {
        const passHash = await this.encryptPassword(user.password);

        const res = await this.conn.query(`INSERT INTO PUBLIC.BANK4ALL_USERS 
        (NAME,EMAIL,PHONE_NUMBER,ADDRESS,NATIONAL_ID,BIRTH_DATE, PASSWORD) VALUES 
        ('${user.name}','${user.email}','${user.phone_number}','${user.address}','${user.national_id}','${user.birth_date}','${passHash}')`);
        
        return res.rows;
    }

    async get(email: string): Promise<UserDTO | undefined> {
        const res = await this.conn.query(`SELECT * FROM PUBLIC.BANK4ALL_USERS WHERE EMAIL='${email}'`);
        return res.rows[0];
    }

    private async encryptPassword(originalPassword: string) {
        let result;

        await new Promise((resolve)=> genSalt(saltRounds, (err,salt)=>{
            hash(originalPassword, salt, (err,hash)=>{
                result = hash;
                resolve(result);
                });
            })
        );

        return result;
    }
}
