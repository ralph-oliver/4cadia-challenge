import { Body, Controller, HttpCode, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { UserDTO } from '../user/dto/user-dto';
import { AuthService } from './auth.service';
import { AuthDTO } from './dto/auth-dto';

@ApiTags('Auth')
@Controller('auth')

export class AuthController {

    constructor(private authService:AuthService) {
    }
    
    @UseGuards(AuthGuard('local'))
    @Post('login')
    @HttpCode(HttpStatus.OK)
    async login(@Body() req:AuthDTO) {
      return this.authService.login(req as UserDTO);
    }
}
