import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { compare } from 'bcrypt';
import { UserDTO } from '../user/dto/user-dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {

    constructor(
      private userService:UserService,
      private jwtService:JwtService) {}

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.userService.get(username);

        if (!user) {
          throw new HttpException({
            status: HttpStatus.BAD_REQUEST,
            error:'Incorrect username or password.'
          }, HttpStatus.BAD_REQUEST);
        }

        let isValid = await this.validatePassword(user,password);
        return isValid? user : null;
    }

    async validatePassword(user: UserDTO, inputtedPassword: string) {
      return await new Promise((resolve)=> compare(inputtedPassword, user.password, (err,isValid)=> {
         resolve(isValid);
        })
      );
    }

    async login(user: UserDTO) {
      const payload = { username: user.email, sub: user.id };
      return {
        access_token: this.jwtService.sign(payload),
        name:user.name,
        email:user.email
      };
    }
}
