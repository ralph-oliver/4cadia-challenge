import { Module } from '@nestjs/common';
import { UserService } from './user/user.service';
import { UserController } from './user/user.controller';
import { DbModule } from 'src/db/db.module';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConsts } from 'src/constants';
import { LocalStrategy } from './auth/passport/local.strategy';

@Module({
  providers: [UserService, AuthService, LocalStrategy],
  imports: [
    DbModule, 
    PassportModule,
    JwtModule.register({
      secret: jwtConsts.secret,
      signOptions:{
        expiresIn:'10m'
      }
    })
  ],
  controllers: [UserController, AuthController]
})
export class UserModule {}
