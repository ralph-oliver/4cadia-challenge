import { HttpModule, Module } from '@nestjs/common';
import { DbModule } from 'src/db/db.module';
import { UserModule } from 'src/modules/user/user.module';
import { UserService } from 'src/modules/user/user/user.service';
import { BankAccountController } from './bank-account.controller';
import { BankAccountService } from './bank-account.service';

@Module({
  controllers: [BankAccountController],
  imports: [DbModule, UserModule, HttpModule],
  providers: [BankAccountService, UserService]
})
export class BankAccountModule {}
