import { HttpException, HttpService, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION } from 'src/constants';
import { UserService } from 'src/modules/user/user/user.service';
import camelcaseKeys = require('camelcase-keys');

const apiUrl = 'localhost';
@Injectable()
export class BankAccountService {

    constructor(@Inject(PG_CONNECTION) private conn: any, private userService:UserService, private http:HttpService) {
    }

    async createBankAccountLink(bankAccountLinkRequest:any, currentUserEmail:string) {

        let accessToken = await this.http.get('http://' + apiUrl + ':3001/requestAccess').toPromise();
        accessToken = accessToken.data;

        const currentUser = await this.userService.get(currentUserEmail);

        const existingAccount = await this.conn.query(`select * from bank4all_accounts where user_id = ${currentUser.id} and account_number =${bankAccountLinkRequest.accountNumber}`);

        if (existingAccount.rows.length > 0) {
            throw new HttpException("ERR_ALREADY_LINKED", HttpStatus.BAD_REQUEST);
        }

        const res = await this.conn.query(
            `insert into bank4all_accounts (user_id,bank_id, account_number, agency_number, access_token) values ('${(await currentUser).id}','${bankAccountLinkRequest.bankId}','${bankAccountLinkRequest.accountNumber}','${bankAccountLinkRequest.agencyNumber}','${accessToken}')`
        )

        return res.rowCount;     
    }

    async getUserAccounts(currentUserEmail:string) {
        const currentUser = await this.userService.get(currentUserEmail);

        const res = await this.conn.query(
            `select a.id, b."name" as bank_name, a.account_number, a.agency_number,a.user_id, a.bank_id, a.access_token from bank4all_accounts a
                inner join bank4all_banks b on a.bank_id  = b.id
            where a.user_id = ${currentUser.id}`
        );

        return camelcaseKeys(res.rows);
    }

    async getAccountData(currentUserEmail:string, accountId:string) {
        const currentUser = await this.userService.get(currentUserEmail);

        const res = await this.conn.query(
            `select a.id, b."name" as bank_name, a.account_number, a.agency_number,a.user_id, a.bank_id, a.access_token from bank4all_accounts a
                inner join bank4all_banks b on a.bank_id  = b.id
            where a.user_id = ${currentUser.id} and a.id = ${accountId}`
        );

        if (res.rows.length > 0) {
            try {
                const bankData = await this.http.get('http://' + apiUrl + ':3001/getBankData/' + res.rows[0].access_token).toPromise();

                res.rows[0].bank_data = bankData.data;
                return JSON.stringify(camelcaseKeys(res.rows[0]));
            }
            catch (error) {
                if (error.response && error.response.status == 401) {
                    throw new HttpException("INVALID_TOKEN", HttpStatus.UNAUTHORIZED);
                }
            }
        }
        else {
            throw new HttpException("No account data found", HttpStatus.NOT_FOUND)
        }
    }

    async getAccountBalance(currentUserEmail:string, accountId:string) {
        var result = await this.getAccountData(currentUserEmail,accountId);
        return JSON.parse(result).bankData.map(x=>x.amount).reduceRight((x,y)=>x+y);
    }

    async newLinkRequest(currentUserEmail:string, accountId:string) {
        let accessToken = await this.http.get('http://' + apiUrl + ':3001/requestAccess').toPromise();
        accessToken = accessToken.data;

        const currentUser = await this.userService.get(currentUserEmail);

        const result = await this.conn.query(`update bank4all_accounts set access_token ='${accessToken}' where user_id = ${currentUser.id} and id =${accountId}`);

        if (result.rowcount < 1) {
            throw new HttpException("No account data found", HttpStatus.NOT_FOUND)
        }

        return result.rowcount;
    }
    
}
