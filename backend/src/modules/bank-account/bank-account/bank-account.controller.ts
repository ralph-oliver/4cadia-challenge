import { Body, Controller, Get, HttpCode, Param, Post, Req, UseGuards } from '@nestjs/common';
import { BankAccountService } from './bank-account.service';
import { JwtAuthGuard } from '../../user/auth/jwt-auth.guard';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { LinkNewBankAccountDTO } from './dto/link-new-bank-account-dto';

@ApiTags('User Bank Account')
@ApiBearerAuth('access-token')
@Controller('bank-account')
export class BankAccountController {

    constructor(private readonly bankAccountService: BankAccountService) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @Post()
    @HttpCode(200)
    async createAccountLink(@Req() req:any, @Body() bankAccount:LinkNewBankAccountDTO) {
        return await this.bankAccountService.createBankAccountLink(bankAccount, req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getUserAccounts(@Req() req:any) {
        return await this.bankAccountService.getUserAccounts(req.user.username);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/transactions/:accountId')
    @ApiParam({name: 'accountId', required: true})
    async getAccountData(@Param('accountId') accountId, @Req() req:any) {
        return await this.bankAccountService.getAccountData(req.user.username, accountId);;
    }

    @UseGuards(JwtAuthGuard)
    @Get('new-link-request/:accountId')
    @ApiParam({name: 'accountId', required: true})
    async makeLinkRequest(@Param('accountId') accountId, @Req() req:any) {
        return await this.bankAccountService.newLinkRequest(req.user.username, accountId);;
    }

    @UseGuards(JwtAuthGuard)
    @Get('/balance/:accountId')
    @ApiParam({name: 'accountId', required: true})
    async getAccountBalance(@Param('accountId') accountId, @Req() req:any) {
        return await this.bankAccountService.getAccountBalance(req.user.username, accountId);;
    }

}


