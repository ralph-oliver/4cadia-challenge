import { ApiProperty } from "@nestjs/swagger";

export class LinkNewBankAccountDTO {

    @ApiProperty()
    accountNumber:string;

    @ApiProperty()
    agencyNumber:string;
    
    @ApiProperty()
    bankId:string;
}
