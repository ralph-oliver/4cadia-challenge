import { Module } from '@nestjs/common';
import { DbModule } from 'src/db/db.module';
import { BankController } from './bank/bank.controller';
import { BankService } from './bank/bank.service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '../user/auth/passport/jwt.strategy';
@Module({
  controllers: [BankController],
  imports: [DbModule, PassportModule],
  providers: [BankService, JwtStrategy]
})
export class BankModule {}
