import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { BankService } from './bank.service';
import { JwtAuthGuard } from '../../user/auth/jwt-auth.guard';

@ApiTags('Bank')
@Controller('bank')
@ApiBearerAuth('access-token')
export class BankController {

    constructor(private readonly bankService: BankService) {}

    @UseGuards(JwtAuthGuard)
    @Get('listAll')
    async listAll(): Promise<any> {
        return await this.bankService.listAll();    
    }
}

